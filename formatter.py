# -*- coding: utf8 -*-
from html.parser import HTMLParser
from re import sub
from sys import stderr
from traceback import print_exc
from config import Config


class HtmlToText(HTMLParser):
    """
    Define our parser based on HTMLParser
    """
    def __init__(self):
        HTMLParser.__init__(self)
        self.__article = []

    def handle_data(self, data):
        article = data.strip()
        if len(article) > 0:
            article = sub('[ \t\r\n]+', ' ', article)
            self.__article.append(article + ' ')

    def handle_starttag(self, tag, attrs):
        if tag == 'p':
            self.__article.append('\n\n')  # add new line instead of <p>
        elif tag == "a":
            # check the list of defined attributes.
            for name, value in attrs:
                # if href is defined, print it.
                if name == "href":
                    self.__article.extend(str('[' + value[:78] + '] '))

    @property
    def article(self):
        return ''.join(self.__article).strip()  # create an article


def wrapped_lines(line, width=80):
    """Wrap data to lines with optional width"""
    whitespace = set(" \n\t\r")
    length = len(line)
    start = 0

    while start < (length - width):
        # we take next 'width' of characters:
        chunk = line[start:start + width + 1]
        # if there is a newline in it, let's return first part
        if '\n' in chunk:
            end = start + chunk.find('\n')
            yield line[start:end]
            start = end + 1  # we set new start on place where we are now
            continue

        # if no newline in chunk, let's find the first whitespace from the end
        for i, ch in enumerate(reversed(chunk)):
            if ch in whitespace:
                end = (start + width - i)
                yield line[start:end]
                start = end + 1
                break
    yield line[start:]


def html_to_text(article):
    """Parse article html, wrap it and return generator"""
    try:
        parser = HtmlToText()
        parser.feed(article)
        parser.close()
        return wrapped_lines(parser.article, int(Config.max_line_lengh))
    except:
        print_exc(file=stderr)
        return article
