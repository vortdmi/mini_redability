# -*- coding: utf8 -*-
"""Mini Readability

    How to use:
        python3 main.py -u <url-to-article>
"""
import os
import errno
import re
from extractor import Extractor
from formatter import html_to_text
from config import Config

ext = Extractor()
article = ext.get_article
article_url = Config.args.url

url = re.compile(r"https?://(www\.)?")
url.sub('', article_url.strip().strip('/'))  # remove 'http/s' from url
filename = url.sub('', article_url.strip().strip('/')) + '.txt'

if __name__ == '__main__':
    # create dirs and text file
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise

    # write article text to file
    with open(filename, "w") as f:
        for line in html_to_text(article):
            f.write(str(line + '\n'))
