# -*- coding: utf8 -*-
import requests
from lxml import html, etree
from config import Config


class Extractor(object):
    """
    Article extractor from news websites.
    """
    page = requests.get(Config.args.url, headers=Config.headers)

    @property
    def get_dom(self):
        """Get whole html dom from url and decode if it possible"""
        try:
            tree = html.fromstring(self.page.content.decode('utf-8'))
        except:
            tree = html.fromstring(self.page.content)
        return tree

    @property
    def get_p_nodes(self):
        """Get all <div> elements with <p> elements >= 1 and make a list"""
        dom = self.get_dom
        nodes = dom.xpath('//div[count(p) >= 1]')
        paragraph_nodes = []
        for node in nodes:
            paragraph_nodes.append(str(etree.tounicode(node)))
        return paragraph_nodes

    @property
    def get_article(self):
        """A lot of <p> elements => 90% this is an article"""
        self.get_p_nodes.sort(key='<p>'.count)
        return self.get_p_nodes[0]
