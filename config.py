# -*- coding: utf8 -*-
import argparse


class Config(object):
    """Config class for an app. Here you can configure Headers,
    Args and Max Line Lengh for output file
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--url', action='store', dest="url",
                        help="Article URL", default="")
    args = parser.parse_args()
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) '
                      'AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/39.0.2171.95 Safari/537.36'
    }
    max_line_lengh = '80'
